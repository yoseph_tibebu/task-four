// example.test.js
const expect = require('chai').expect;
const {assert}= require('chai');
const mylib = require('../src/mylib');
describe('Unit testing mylib.js', () => {
    const n1 = 5;
    const n2 = 4;
    const res = 20;
    before(()=>{

        assert.isNotString(n1)
        assert.isNotString(n2)
        
    })    
it('Should return full name based on first and last name',() => {
const result = mylib.fullName("Yoseph","Tibebu")
expect(result).to.equal("Yoseph Tibebu") 
  

})

it('Should return the product of the numbers',() => {
    
    const result = mylib.multiplyNumbers(n1,n2)
    expect(result).to.equal(res) 
    })

    after(()=> console.log("Done with the test"))    

});


